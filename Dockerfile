FROM alpine:3.12

RUN apk add --no-cache \
    tzdata \
    py3-pip \
    py3-numpy \
    py3-pillow \
    py3-toml \
    py3-dateutil \
    gphoto2 \
    udev

RUN pip3 install --upgrade pip && \
    pip3 install \
        pytelegraf[http] \
        structlog \
        pyudev

# the READTHEDOCS hack is nuts
RUN if [[ $(uname -m) == "armv7l" ]] || [[ $(uname -m) == "armv6l" ]];then \
        export READTHEDOCS=True && \
        apk add --no-cache \
            raspberrypi \
            raspberrypi-libs \
            raspberrypi-dev && \
        pip3 install picamera[array]; \
    else echo "Running on $(uname -m), not armv6l or armv7l, not installing rpi stuff";fi

# this is required to work on rapsberry pi :/
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/vc/lib

WORKDIR /app

COPY LICENSE .
COPY libeyepi libeyepi
COPY fonts fonts
COPY run.py .

ENTRYPOINT [ "python3" ]
CMD ["run.py"]

