#!/usr/bin/env python3
import datetime
from toml.decoder import TomlDecoder
import time
import toml
import socket
import re
from threading import Lock
from libeyepi import GPCamera
import pyudev
import sys
import subprocess
import logging
import structlog
import os


lg = logging.getLogger("_")
lg.setLevel(logging.DEBUG)


formatter = structlog.stdlib.ProcessorFormatter(
    processor=structlog.processors.JSONRenderer(),
    foreign_pre_chain=[
        structlog.threadlocal.merge_threadlocal,
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_log_level_number,
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.UnicodeDecoder()
    ],
    logger=lg
)


handler = logging.StreamHandler(stream=sys.stdout)
handler.setFormatter(formatter)
logging.basicConfig(level=logging.INFO,
                    handlers=[handler])

structlog.configure(
    processors=[
        structlog.stdlib.PositionalArgumentsFormatter(),
        lambda _,__, ed: {'msg': ed.pop("event"), 'stack_info': ed.pop('stack_info', None),'exc_info': ed.pop('exc_info', None),'extra': ed}
    ],
    # context_class=dict,
    wrapper_class=structlog.stdlib.BoundLogger,
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=True,
)

logger = structlog.getLogger()

has_picamera = False
try:
    from libeyepi import PiCamera
    has_picamera = True
except Exception as e:
    logger.error("Couldnt import picamera module, no picamera camera support:", exc_info=e)
    pass


__author__ = "Gareth Dunstone"
__copyright__ = "Copyright 2018, Borevitz Lab"
__credits__ = ["Gareth Dunstone", "Tim Brown", "Justin Borevitz", "Kevin Murray", "Jack Adamson"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Gareth Dunstone"
__email__ = "gareth.dunstone@anu.edu.au"
__status__ = "alpha"


# global recent
# recent = time.time()
# global glock
# glock = Lock()
# global workers
# workers = []


def detect_picam(conf) -> tuple:
    """
    Detects the existence of a picam
    on all SPC-OS devices this will return true if the picam is installed
    on other rpis it may return false if the raspberrypi-firmware-tools is not installed or the boot.cfg flag
    for the camera is not set.

    todo: this shoud return an empty tuple if an ivport is detected.
    todo: clean this up so that it doesnt require subprocess.

    :creates: :mod:`libs.Camera.PiCamera`, :mod:`libs.Uploader.Uploader`
    :param updater: instance that has a `communication_queue` member that implements an `append` method
    :type updater: Updater
    :return: tuple of raspberry pi camera thread and uploader.
    :rtype: tuple(PiCamera, Uploader)
    """
    logger.debug("detecting picamera")
    if "name" not in conf:
        conf['name'] = "{}-Picam".format(socket.gethostname())

    if not (os.path.exists("/opt/vc/bin/vcgencmd")):
        logger.error("vcgencmd not found, cannot detect picamera.")
        return tuple()
    try:
        cmdret = subprocess.check_output("/opt/vc/bin/vcgencmd get_camera", shell=True).decode()
        if "detected=1" in cmdret:
            camera = PiCamera.PiCamera(conf)
            return camera,
        else:
            return tuple()
    except subprocess.CalledProcessError as e:
        logger.error("Couldn't detect picamera. Error calling vcgencmd.", exc_info=e)
        pass
    except Exception as e:
        logger.error("General Exception in picamera detection.", exc_info=e)
    return tuple()


def detect_gphoto_info():
    """
    detects cameras connected via gphoto2 command line.

    this can potentially cause long wait times if a camera is attempting to capture for the split second that it tries
    to gphoto2, however it is the preferred method because it is the most robust and comparmentalised.

    :param type:
    :return: a dict of port:serialnumber values corresponding to the currently connected gphoto2 cameras.
    """

    try:
        cams = {}
        # check output of the --auto-detect gphoto2 command.
        detect_ret = subprocess.check_output(["/usr/bin/gphoto2",
                                              "--auto-detect"],
                                             universal_newlines=True)
        # iterate over the results, should be in the format of [(bus, addr), (bus, addr) ...] for usb connected dslrs
        # this regex matches occurrences of "usb:" followed by 2 comma separated digits.
        for bus, addr in re.findall(r'usb:(\d+),(\d+)', detect_ret):
            try:
                # findall returns strings...
                bus, addr = int(bus), int(addr)
                # this is the format that gphoto2 expects the port to be in.
                port = "usb:{0:03d},{1:03d}".format(bus, addr)

                # gphoto2 command to get the serial number for the DSLR
                # WARNING: when the port here needs to be correct, because otherwise gphoto2 will return values from
                # an arbitrary camera
                sn_detect_ret = subprocess.check_output(['/usr/bin/gphoto2',
                                                         '--port={}'.format(port),
                                                         '--get-config=serialnumber'],
                                                        universal_newlines=True)

                # Match the serial number.
                # this regex can also be used to parse the values from --get-config as all results are returned like
                # this:
                # Label: Serial Number
                # Type: TEXT
                # Current: 4fffa81fed8f40d286a63fce62598ef0
                sn_match = re.search(r'Current: (\w+)', sn_detect_ret)

                if not sn_match:
                    # we didnt match any output from the command
                    logger.error("Couldnt match serial number pattern from gphoto2 output for port {}".format(port))
                    continue
                sn = sn_match.group(1)

                if sn.lower() == 'none':
                    # there is a bug in a specific version of gphoto2 that causes it to return 'None'
                    # for the camera serial
                    # number. If we cant get a unique serial number, we are screwed for multicamera
                    # todo: allow this if there is only one camera.
                    logger.error("serial number matched with value of 'none' {}".format(port))
                    continue

                cams[sn] = (bus, addr)
            except Exception as e:
                logger.error("gphoto2 individual camera detection error", exc_info=e)
        return cams
    except subprocess.CalledProcessError as e:
        logger.error("gphoto2 camera detection subprocess error", exc_info=e)

    return dict()


def detect_gphoto(confs, lock):
    """
    detects cameras connected via gphoto2 command line.

    this can potentially cause long wait times if a camera is attempting to capture for the split second that it tries
    to gphoto2, however it is the preferred method because it is the most robust and comparmentalised.

    :param type:
    :return: a dict of port:serialnumber values corresponding to the currently connected gphoto2 cameras.
    """

    logger.info("Detecting gphoto2 cameras")

    try:

        workers = []
        # check output of the --auto-detect gphoto2 command.
        cams = detect_gphoto_info()
        logger.info(f"detected the following gphoto cameras {cams}")
        for name, conf in confs.items():
            try:
                name = conf.get("name", name)
                if conf.get('force', None):
                    camera = GPCamera.GPCamera(conf, lock=lock)
                    workers.append(camera)
                    logger.info(f"forced detection for {name}:{conf.get('gphotoserialnumber', '<no-serial>')}")
                    continue
                if 'gphotoserialnumber' not in conf.keys() and len(confs) > 1:
                    logger.error("gphoto config with no 'gphotoserialnumber', but have multiple cameras")
                    continue
                if conf['gphotoserialnumber'] not in cams.keys():
                    logger.error(f"config with gphotoserialnumber {conf['gphotoserialnumber']} and no matching camera")
                    continue
                # todo: why does this work even without passing a Lock()?
                camera = GPCamera.GPCamera(conf, lock=lock)
                workers.append(camera)
                logger.info(f"Sucessfully detected {conf.get('name', '')}:{camera.serial_number} @ {camera.usb_address}")
            except Exception as e:
                logger.debug("Exception detecting cameras!", exc_info=e)
                continue
        return tuple(workers)
    except subprocess.CalledProcessError:
        logger.error("Subprocess error detecting gphoto2 cameras", exc_info=e)
    return tuple()


# def detect_webcam() -> tuple:
#     """
#     Detects usb web camers using the video4linux pyudev subsystem.
#
#     i.e. if the camera shows up as a /dev/videoX device, it sould be detected here.
#
#     :creates: :mod:`libs.Camera.USBCamera`, :mod:`libs.Uploader.Uploader`
#     :param updater: instance that has a `communication_queue` member that implements an `append` method
#     :type updater: Updater
#     :return: tuple of camera thread objects and associated uploader thread objects.
#     :rtype: tuple(USBCamera, Uploader)
#     """
#     try:
#         logger.info("Detecting USB web cameras.")
#         workers = []
#         for device in pyudev.Context().list_devices(subsystem="video4linux"):
#             serial = device.get("ID_SERIAL_SHORT", None)
#             if not serial:
#                 serial = device.get("ID_SERIAL", None)
#                 if len(serial) > 6:
#                     serial = serial[:6]
#                 logger.info("Detected USB camera. Using default machine id serial {}".format(str(serial)))
#             else:
#                 logger.info("Detected USB camera {}".format(str(serial)))
#
#             identifier = "USB-{}".format(serial)
#             sys_number = device.sys_number
#
#             try:
#                 # logger.warning("adding {} on {}".format(identifier, sys_number))
#                 camera = Camera.USBCamera(identifier=identifier,
#                                    sys_number=sys_number,
#                                    queue=updater.communication_queue)
#                 updater.add_to_temp_identifiers(camera.identifier)
#                 workers.append(camera)
#                 workers.append(Uploader(identifier, queue=updater.communication_queue))
#             except Exception as e:
#                 logger.error("Unable to start usb webcamera {} on {}".format(identifier, sys_number))
#                 logger.error(", exc_info=e)
#         return start_workers(tuple(workers))
#     except Exception as e:
#         logger.error("couldnt detect the usb cameras", exc_info=e)
#     return tuple()


DURATION_RE = re.compile(r"(?:([0-9]+|[0-9]+\.[0-9]+)(ms|us|μs|[wdhms]))")


def parse_duration(v):
    duration_params = dict()
    mapping = {
        'w': "weeks",
        'd': "days",
        'h': "hours",
        'm': "minutes",
        's': "seconds",
        'ms': "milliseconds",
        'us': "microseconds",
        'μs': "microseconds"
    }
    for mtch in DURATION_RE.finditer(v):
        v, u = mtch.groups()

        full_unit = mapping.get(u, None)
        if full_unit is None:
            continue

        if full_unit in duration_params.keys():
            duration_params[full_unit] += float(v)
        else:
            duration_params[full_unit] = float(v)

    return datetime.timedelta(**duration_params)


def force_load_value(v):
    if str(v).lower() in ("true", "yes", "on", "t"):
        return True, "bool"
    if str(v).lower() in ("false", "no", "off", "f"):
        return False, "bool"
    try:
        return int(v), "int"
    except Exception:
        pass
    try:
        return float(v), "float"
    except Exception:
        pass
    return str(v), "str"


class TomlDecoderWithDuration(TomlDecoder):
    def load_value(self, v, strictly_valid=True):
        if not v:
            raise ValueError("Empty value is invalid")
        if str(v).lower() in ("true", "yes", "on", "t"):
            return True, "bool"
        if str(v).lower() in ("false", "no", "off", "f"):
            return False, "bool"
        if v[0] not in "[{\"\'" and DURATION_RE.match(v):
            return parse_duration(v), "duration"
        try:
            return super().load_value(v, strictly_valid=strictly_valid)
        except Exception:
            return force_load_value(v)


def get_env(prefix):
    return dict((x.replace(prefix, "", 1), y) for x, y in os.environ.items() if x.startswith(prefix))


def override_conf(env_prefix, conf=None):
    if conf is None:
        conf = dict()
    decoder = TomlDecoderWithDuration()
    for k, v in get_env(env_prefix).items():
        key = k.lower()
        conf[key], _ = decoder.load_value(v)
    return conf


CONFIG_RE = re.compile(r'.*(?P<type>GPHOTO|gphoto|USB|usb|GIGE|gige|PICAMERA|picamera)_(?P<name>[A-Za-z0-9]*)_.*=?')


def build_configs_from_env():
    all_env = get_env("EYEPI_")
    configs = {}
    config_keys = dict()
    for k in all_env.keys():
        match = CONFIG_RE.match(k)
        if match:
            mdict = match.groupdict()
            config_key = f"EYEPI_{mdict['type']}_{mdict['name']}_"
            config_keys[config_key] = (mdict['type'].lower(), mdict['name'].lower())
    for config_key, (config_type, config_name) in config_keys.items():
        if config_type not in configs.keys():
            configs[config_type] = dict()
        if config_name not in configs[config_type].keys():
            configs[config_type][config_name] = dict()
        configs[config_type][config_name] = override_conf(config_key, conf=configs[config_type][config_name])
        if "name" not in configs[config_type][config_name].keys():
            configs[config_type][config_name]['name'] = config_name
            logger.warn(f"using config name ({config_name}) as camera name, this is not recommended")
        logger.debug(f"loaded config from env: {configs[config_type][config_name]}")
    return configs


def run_from_config(lock):
    try:
        config = toml.load("/etc/eyepi/eyepi.conf", decoder=TomlDecoderWithDuration)
    except Exception as e:
        logger.warn("couldn't load eyepi.conf", exc_info=e)
        logger.info("loading config fully from environment...")
        config = build_configs_from_env()

    local_workers = []
    if has_picamera:
        rpiconfs = config.get("picamera", {})
        if len(rpiconfs) > 1:
            logger.error("multiple picameras are not supported, not running any picameras. check config")
            rpiconfs = {}
        for key, conf in rpiconfs.items():
            if len(rpiconfs) > 0:
                conf = override_conf(f"EYEPI_PICAMERA_{key}_", conf=conf)
                rpi = detect_picam(conf)
                local_workers.extend(rpi)
    else:
        logger.warn("no picamera module!")

    gphoto_confs = config.get("gphoto", {})
    if len(gphoto_confs) > 0:
        overridden_gphotoconfs = {}
        for key, conf in gphoto_confs.items():
            overridden_gphotoconfs[key] = override_conf(f"EYEPI_GPHOTO_{key}_", conf=conf)
        gphoto_cameras = detect_gphoto(overridden_gphotoconfs, lock)
        local_workers.extend(gphoto_cameras)
    return start_workers(local_workers)


def enumerate_usb_devices() -> set:
    """
    Gets a set of the current usb devices from pyudev

    :return: set of pyudev usb device objects
    :rtype: set(pyudev.Device)
    """
    return set(pyudev.Context().list_devices(subsystem="usb"))


def start_workers(worker_objects: tuple or list) -> tuple:
    """
    Starts threaded workers

    :param worker_objects: tuple of worker objects (threads)
    :return: tuple of started worker objects
    :rtype: tuple(threading.Thread)
    """
    logger.debug("Starting {} worker threads".format(str(len(worker_objects))))
    for thread in worker_objects:
        try:
            thread.daemon = True
            thread.start()
        except Exception as e:
            logger.error("error starting worker threads", exc_info=e)
            raise e
    return worker_objects


def kill_workers(worker_objects: tuple):
    """
    stops all workers

    calls the stop method of the workers (they should all implement this as they are threads).

    :param worker_objects:
    :type worker_objects: tuple(threading.Thread)
    """
    logger.debug("Killing {} worker threads".format(str(len(worker_objects))))
    for thread in worker_objects:
        thread.stop()


def main():
    logger.info("Program startup...")
    try:
        import pkg_resources
        logger.info("py-eyepi: {}".format(pkg_resources.get_distribution("py-eyepi").version))
    except Exception:
        logger.warn("couldn't determine py-eyepi version")
    # The main loop for capture

    workers = tuple()
    lock = Lock()
    try:
        # global recent
        # # start the updater. this is the first thing that should happen.
        # recent = time.time()
        try:
            workers = run_from_config(lock)
        except Exception as e:
            logger.fatal("fatal error in thread startup", exc_info=e)
        # enumerate the usb devices to compare them later on.
        # global glock
        # glock = Lock()
        #
        # def recreate(action, event):
        #     # these all need to be "globalised"
        #     global glock
        #     global workers
        #     global recent
        #     try:
        #         # use manual global lock.
        #         # this callback is from the observer thread, so we need to lock shared resources.
        #         if "gpio" in event.sys_name:
        #             return
        #
        #         if time.time() - 10 > recent and action in ['config_change', "add", "remove"]:
        #             with glock:
        #                 logger.warning("recreating workers, {}".format(action))
        #                 kill_workers(workers)
        #                 workers = run_from_config()
        #                 recent = time.time()
        #     except Exception as e:
        #         logger.fatal("exception recreating workers", exc_info=e)
        #
        # context = pyudev.Context()
        # monitor = pyudev.Monitor.from_netlink(context)
        # observer = pyudev.MonitorObserver(monitor, recreate)
        # observer.start()

        while True:
            try:
                time.sleep(60 * 60 * 12)
                logger.info("recreating workers")
                kill_workers(workers)
                workers = run_from_config(lock)
            except (KeyboardInterrupt, SystemExit) as e:
                kill_workers(workers)
                raise e
            except Exception as e:
                logger.fatal("fatal error in main loop", exc_info=e)

    except (KeyboardInterrupt, SystemExit):
        logger.info("exiting normally")
        kill_workers(workers)
        sys.exit()
    except Exception as e:
        logger.fatal("fatal error in main", exc_info=e)


if __name__ == "__main__":
    main()
