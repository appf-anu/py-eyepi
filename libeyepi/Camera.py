import datetime

import os
import shutil
import time
import tempfile
from dateutil import parser

from urllib.parse import urlparse
from threading import Thread, Event
from PIL import Image, ImageDraw, ImageFont
import structlog

baselogger = structlog.getLogger("Camera Base")

try:
    import telegraf
except Exception as e:
    baselogger.error("Couldnt import pytelegraf module, no telemetry", exc_info=e)


class TwentyFourHourTimeParserInfo(parser.parserinfo):
    def validate(self, res):
        if res.year is not None:
            time = str(res.year)
            res.year = None
            res.hour = int(time[:2])
            res.minute = int(time[2:])
        if res.tzoffset == 0 and not res.tzname or res.tzname == 'Z':
            res.tzname = "UTC"
            res.tzoffset = 0
        elif res.tzoffset != 0 and res.tzname and self.utczone(res.tzname):
            res.tzoffset = 0
        return True


DEFAULT_OUTPUT_IMAGE_TYPES = ['jpg']
DEFAULT_OUTPUT_IMAGE_RAW_TYPES = ['tiff']
ALL_OUTPUT_IMAGE_TYPES = ["cr2", "raw", "nef", "jpg", "jpeg", "ppm", "tif", "tiff"]
ALL_OUTPUT_IMAGE_RAW_TYPES = ["cr2", "raw", "nef", "tif", "tiff"]


def td_format(td_object):
    seconds = int(td_object.total_seconds())
    periods = [
        ('year', 60 * 60 * 24 * 365),
        ('month', 60 * 60 * 24 * 30),
        ('day', 60 * 60 * 24),
        ('hour', 60 * 60),
        ('minute', 60),
        ('second', 1)
    ]

    strings = []
    for period_name, period_seconds in periods:
        if seconds > period_seconds:
            period_value, seconds = divmod(seconds, period_seconds)
            has_s = 's' if period_value > 1 else ''
            strings.append("%s %s%s" % (period_value, period_name, has_s))

    return ", ".join(strings)


def flatten(o):
    rt = []
    for i in o:
        if isinstance(i, list):
            rt.extend(flatten(i))
        else:
            rt.append(i)
    return rt


class Camera(Thread):
    """
    Base Camera class.
    """

    accuracy = 3

    def __init__(self, config, **kwargs):
        """
        Initialiser for cameras...

        :param identifier: unique identified for this camera, MANDATORY
        :param config: Configuration section for this camera.
        :param queue: deque to push info into
        :param noconf: dont create a config, or watch anything. Used for temporarily streaming from a camera
        :param kwargs:
        """
        identifier = config['name']
        self.logger = structlog.getLogger(f"camera.{identifier}")

        super().__init__(name=identifier)
        # print("Thread started {}: {}".format(self.__class__, identifier))

        self.logger.debug("thread init")

        self.stopper = Event()
        self.identifier = identifier
        self.name = identifier
        self._image = Image.new('RGB', (1, 1))

        self.config = config.copy()
        self.name = self.config.get("name", identifier)

        self.interval = self.config.get("interval", datetime.timedelta(minutes=10))

        self.output_directory = self.config.get("output_directory", f"/var/lib/eyepi/{str(self.identifier)}")
        self.output_rawest_only = self.config.get("output_rawest_only", False)
        self.output_last_image = self.config.get("output_last_image", True)

        # self.begin_capture = datetime.time(0, 0)
        # self.end_capture = datetime.time(23, 59)
        #
        # try:
        #     self.begin_capture = parser.parse(str(self.config["starttime"]),
        #                                       parserinfo=TwentyFourHourTimeParserInfo()).time()
        # except Exception as e:
        #     self.logger.error("Time conversion error starttime - {}".format(str(e)))
        # try:
        #     # cut string to max of 4.
        #     self.end_capture = parser.parse(str(self.config["stoptime"]),
        #                                     parserinfo=TwentyFourHourTimeParserInfo()).time()
        # except Exception as e:
        #     self.logger.error("Time conversion error stoptime - {}".format(str(e)))

        try:
            if not os.path.exists(self.output_directory):
                self.logger.debug(f"recursively creating local output dir {self.output_directory}")
                os.makedirs(self.output_directory)
        except Exception as e:
            self.logger.error(f"error creating local output dir {self.output_directory}", exc_info=e)

        # self.logger.info("Capturing from {} to {}".format(self.begin_capture.strftime("%H:%M"),
        #                                                   self.end_capture.strftime("%H:%M")))
        # self.logger.debug(f"interval: {td_format(self.interval)}")

        self.current_capture_time = datetime.datetime.now()

    def capture_image(self, filename: str = None):
        """
        Camera capture method.
        override this method when creating a new type of camera.

        Behavior:
            - if filename is a string, write images to disk as filename.ext, and return the names of the images written sucessfully.
            - if filename is None, it will set the instance attribute `_image` to a numpy array of the image and return that.

        :param filename: image filename without extension
        :return: :func:`numpy.array` if filename not specified, otherwise list of files.
        :rtype: numpy.array or list(str)
        """
        return self._image

    def capture(self, filename: str = None):
        """
        capture method, only extends functionality of :func:`Camera.capture` so that testing with  can happen

        For extending the Camera class override the Camera.capture_image method, not this one.

        :param filename: image filename without extension
        :return: :func:`numpy.array` if filename not specified, otherwise list of files.
        :rtype: numpy.array
        """

        if filename:
            dirname = os.path.dirname(filename)
            os.makedirs(dirname, exist_ok=True)
        return self.capture_image(filename=filename)

    @staticmethod
    def timestamp(tn: datetime.datetime) -> str:
        """
        Creates a properly formatted timestamp from a datetime object.

        :param tn: datetime to format to timestream timestamp string
        :return: formatted timestamp.
        """
        return tn.strftime('%Y_%m_%d_%H_%M_%S')

    @staticmethod
    def directory_timestamp(tn: datetime.datetime) -> str:
        """
        Creates a properly formatted directory timestamp from a datetime object.

        :param tn: datetime to format to timestream timestamp string
        :return: formatted timestamp.
        """
        return tn.strftime('%Y/%Y_%m/%Y_%m_%d/%Y_%m_%d_%H')

    @staticmethod
    def time2seconds(t: datetime.datetime) -> int:
        """
        Converts a datetime to an integer of seconds since epoch

        :return: integer of seconds since 1970-01-01
        :rtype: int
        """
        try:
            return int(t.timestamp())
        except:
            # the 'timestamp()' method is only implemented in python3.3`
            # this is an old compatibility thing
            return int(t.hour * 60 * 60 + t.minute * 60 + t.second)

    @property
    def timestamped_imagename(self) -> str:
        """
        Builds a timestamped image basename without extension from :func:`Camera.current_capture_time`

        :return: image basename
        :rtype: str
        """
        return '{camera_name}_{timestamp}'.format(camera_name=self.name,
                                                  timestamp=Camera.timestamp(self.current_capture_time))

    @property
    def time_to_capture(self) -> bool:
        """
        Filters out times for capture.

        returns True by default.

        returns False if the conditions where the camera should capture are NOT met.

        :return: whether or not it is time to capture
        :rtype: bool
        """

        # current_naive_time = self.current_capture_time.time()
        # if self.begin_capture < self.end_capture:
        #     # where the start capture time is less than the end capture time
        #     if not self.begin_capture <= current_naive_time <= self.end_capture:
        #         return False
        # else:
        #     # where the start capture time is greater than the end capture time
        #     # i.e. capturing across midnight.
        #     if self.end_capture <= current_naive_time <= self.begin_capture:
        #         return False

        # capture interval
        if not (self.time2seconds(self.current_capture_time) % self.interval.total_seconds() < Camera.accuracy):
            return False
        return True

    def encode_write_image(self, img: Image, fn: str) -> list:
        """
        takes an image from PIL and writes it to disk as a tif and jpg
        converts from rgb to bgr for cv2 so that the images save correctly
        also tries to add exif data to the images

        :param PIL.Image img: 3 dimensional image array, x,y,rgb
        :param str fn: filename
        :return: files successfully written.
        :rtype: list(str)
        """
        # output types must be valid!
        fnp = os.path.splitext(fn)[0]
        successes = list()
        for ext in (DEFAULT_OUTPUT_IMAGE_RAW_TYPES if self.output_rawest_only else DEFAULT_OUTPUT_IMAGE_TYPES):
            fn = "{}.{}".format(fnp, ext)
            try:
                if ext == "tiff" or ext == "tif":
                    # format="TIFF" and compression='tiff_lzw' are required
                    # without these 2 params it will save a tiff without
                    os.makedirs(os.path.dirname(fn), exist_ok=True)
                    img.save(fn, format="TIFF", compression='tiff_lzw')
                else:
                    os.makedirs(os.path.dirname(fn), exist_ok=True)
                    img.save(fn)
                successes.append(fn)
            except Exception as e:
                self.logger.error("couldnt write image", exc_info=e, stack_info=True)

            # im = Image.fromarray(np.uint8(img))
            # s = cv2.imwrite(fn, img)
        return successes

    def stop(self):
        """
        Stops the capture thread, if self is an instance of :class:`threading.Thread`.
        """
        self.stopper.set()

    def focus(self):
        """
        AutoFocus trigger method.
        Unimplemented.
        """
        pass

    def run(self):
        """
        Main method. continuously captures and stores images.
        """
        while True and not self.stopper.is_set():
            self.current_capture_time = datetime.datetime.now()
            if self.time_to_capture:
                telemetry = dict()
                try:
                    if self.config.get("enable", True):
                        with tempfile.TemporaryDirectory(prefix=self.name) as spool:
                            start_capture_time = time.time()
                            raw_image = self.timestamped_imagename

                            self.logger.info("capture begin")
                            # capture. if capture didnt happen dont continue with the rest.
                            tf = self.capture(filename=os.path.join(spool, raw_image))
                            telemetry["timing_capture_s"] = float(time.time() - start_capture_time)
                            if tf is None:
                                self.logger.error("capture error, see camera specific exception")
                                continue

                            files = flatten([tf])

                            telemetry["num_files_created"] = len(files)

                            jpeg = next(
                                iter(filter(lambda f: '.jpeg' in f.lower() or ".jpg" in f.lower(), files)),
                                None)

                            if self.output_last_image and jpeg is not None:
                                st = time.time()
                                img = Image.open(jpeg)
                                img.thumbnail((1024, 768), resample=Image.NEAREST)

                                draw = ImageDraw.Draw(img)

                                draw.text((20, img.size[1] - 100), self.timestamped_imagename, fill=(0, 0, 255),
                                          font=ImageFont.truetype("fonts/Inconsolata-Bold.ttf", 50))

                                last_image_fn = os.path.join(self.output_directory, "last_image.jpg")
                                last_image_dn = os.path.dirname(last_image_fn)
                                os.makedirs(last_image_dn, exist_ok=True)
                                img.save(last_image_fn)
                                del img, draw
                                resize_t = time.time() - st

                                telemetry["timing_resize_s"] = float(resize_t)
                                self.logger.debug("Resize {0:.3f}s, total: {0:.3f}s".format(resize_t, time.time() - st))

                            for fn in files:
                                # move files to the upload directory
                                try:
                                    out_dir = os.path.join(self.output_directory,
                                                           Camera.directory_timestamp(self.current_capture_time))
                                    os.makedirs(out_dir, exist_ok=True)
                                    shutil.move(fn, out_dir)
                                    self.logger.info("Captured & stored {}".format(os.path.basename(fn)))
                                except Exception as e:
                                    self.logger.error("Couldn't move timstamped image", exc_info=e, stack_info=True)

                                # remove the spooled files that remain
                                try:
                                    if os.path.isfile(fn):
                                        self.logger.warn("File remaining in spool directory, removing: {}".format(fn))
                                        os.remove(fn)
                                except Exception as e:
                                    self.logger.error("Couldn't remove spooled when it still exists",
                                                      exc_info=e,
                                                      stack_info=True)
                            # log total capture time
                            total_capture_time = time.time() - start_capture_time
                            self.logger.debug("Total capture time: {0:.2f}s".format(total_capture_time))
                            telemetry["timing_total_s"] = float(total_capture_time)
                            # communicate our success with the updater
                            try:
                                telegraf_host_str = os.environ.get("TELEGRAF_HOST", "udp://telegraf")
                                telegraf_host = urlparse(telegraf_host_str)

                                if telegraf_host.scheme == "tcp":
                                    telegraf_client = telegraf.HttpClient(host=telegraf_host.hostname,
                                                                          port=telegraf_host.port or 8186)
                                elif telegraf_host.scheme in ("udp", ""):
                                    telegraf_client = telegraf.TelegrafClient(host=telegraf_host.hostname,
                                                                              port=telegraf_host.port or 8092)
                                else:
                                    raise ValueError("this project only supports tcp:// and udp:// schemes for telegraf, not {}"
                                                     .format(telegraf_host.scheme))
                                telegraf_client.metric("camera", telemetry, tags={"camera_name": self.name})
                                self.logger.debug("Communicated telemetry to telegraf")
                            except Exception as e:
                                self.logger.error("Couldnt communicate with telegraf client.", exc_info=e, stack_info=True)
                            self.logger.info("capture success")
                            # sleep for a little bit so we dont try and capture again so soon.
                            time.sleep(Camera.accuracy * 2)
                except Exception as e:
                    self.logger.critical("image capture thread error in main loop", exc_info=e, stack_info=True)
            time.sleep(1)
