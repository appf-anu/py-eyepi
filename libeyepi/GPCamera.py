import glob
import subprocess
import re
import traceback
import os
import logging
from .Camera import Camera
from threading import Lock
from PIL import Image


class GPCamera(Camera):
    """
    Camera class
    other cameras inherit from this class.
    identifier and usb_address are NOT OPTIONAL
    """

    def __init__(self, config, lock=Lock(), **kwargs):
        """
        Providing a usb address and no identifier or an identifier but no usb address will cause

        :param identifier:
        :param lock:
        :param usb_address:
        :param kwargs:
        """

        self.lock = lock
        self.usb_address = [None, None]
        # we need to do this stuff before the thread starts:
        identifier = config['name']
        self._serialnumber = config['gphotoserialnumber']
        self.identifier = identifier
        super().__init__(config, **kwargs)

        self.usb_address = self.usb_address_detect()

        self.logger.info("gphoto camera detect/init usb port {}:{}".format(*self.usb_address))

    def usb_address_detect(self) -> tuple:
        with self.lock:
            detect_ret = subprocess.check_output(["/usr/bin/gphoto2",
                                                  "--auto-detect"],
                                                 universal_newlines=True)
            detected_usb_ports = re.findall(r'usb:(\d+),(\d+)', detect_ret)
            for bus, addr in detected_usb_ports:
                try:
                    # findall returns strings...
                    bus, addr = int(bus), int(addr)
                    # this is the format that gphoto2 expects the port to be in.
                    port = "usb:{},{}".format(bus, addr)

                    # gphoto2 command to get the serial number for the DSLR
                    # WARNING: when the port here needs to be correct, because otherwise gphoto2 will return values from
                    # an arbitrary camera
                    sn_detect_ret = subprocess.check_output(['/usr/bin/gphoto2',
                                                             '--port={}'.format(port),
                                                             '--get-config=serialnumber'],
                                                            universal_newlines=True)

                    # Match the serial number.
                    # this regex can also be used to parse the values from --get-config as all results are returned like this:
                    # Label: Serial Number
                    # Type: TEXT
                    # Current: 4fffa81fed8f40d286a63fce62598ef0
                    sn_match = re.search(r'Current: (\w+)', sn_detect_ret)

                    if not sn_match:
                        # we didnt match any output from the command
                        self.logger.error("couldn't match serial number pattern from gphoto2 output for port {}".format(port))
                        continue
                    sn = sn_match.group(1)

                    if sn.lower() == 'none':
                        # there is a bug in a specific version of gphoto2 that causes it to return 'None' for the camera serial
                        # number. If we cant get a unique serial number, we are screwed for multicamera
                        # todo: allow this if there is only one camera.
                        self.logger.error("serial number matched with value of 'none' {}".format(port))
                        continue

                    # pad the serialnumber to 32
                    if self._serialnumber == sn:
                        return bus, addr

                except Exception as e:
                    raise e

            else:
                raise ValueError(f"No camera from {detected_usb_ports} matched {self._serialnumber}")


    def capture_image(self, filename=None):
        """
        Gapture method for DSLRs.
        Some contention exists around this method, as its definitely not the easiest thing to have operate robustly.
        :func:`GPCamera._cffi_capture` is how it _should_ be done, however that method is unreliable and causes many
        crashes when in real world timelapse situations.
        This method calls gphoto2 directly, which makes us dependent on gphoto2 (not just libgphoto2 and gphoto2-cffi),
        and there is probably some issue with calling gphoto2 at the same time like 5 times, maybe dont push it.

        :param filename: filename without extension to capture to.
        :return: list of filenames (of captured images) if filename was specified, otherwise a numpy array of the image.
        :rtype: numpy.array or list
        """

        # the %C filename parameter given to gphoto2 will automatically expand the number of image types that the
        # camera is set to capture to.

        # this one shouldnt really be used.
        fn = "{}-temp.%C".format(self.name)
        if filename:
            # if target file path exists
            fn = os.path.join(self.output_directory, "{}.%C".format(filename))

        try:
            self.usb_address = self.usb_address_detect()
        except Exception as e:
            self.logger.error("exception re-detecting gphoto2 camera usb port", exc_info=e, stack_info=True)
            return None

        cmd = [
            "gphoto2",
            "--port=usb:{bus:03d},{dev:03d}".format(bus=self.usb_address[0], dev=self.usb_address[1]),
            "--set-config=capturetarget=0",  # capture to sdram
            "--force-overwrite",  # if the target image exists. If this isnt present gphoto2 will lock up asking
            "--capture-image-and-download",  # must capture & download in the same call to use sdram target.
            '--filename={}'.format(fn)
        ]
        with self.lock:
            self.logger.info("gphoto camera capture begin")
            for tries in range(6):
                self.logger.debug("command: {}".format(" ".join(cmd)))
                try:
                    output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)

                    if "error" in output.lower():
                        raise subprocess.CalledProcessError("non-zero exit status", cmd=cmd, output=output)
                    else:
                        # log success of capture
                        self.logger.info("gphoto camera capture success")
                        self.logger.debug("gphoto2_output: \n" + output)

                        # glob up captured images
                        filenames = glob.glob(fn.replace("%C", "*"))
                        # if there are no captured images, log the error
                        if not len(filenames):
                            self.logger.error("gphoto camera capture resulted in no files")
                        else:
                            # try and load an image for the last_image.jpg resized doodadery
                            try:
                                jpeg = next(iter(filter(lambda h: '.jpeg' in h.lower() or ".jpg" in h.lower(), filenames)), None)
                                if jpeg:
                                    # self._image = Image.open(jpeg)
                                    if self.output_rawest_only and len(filenames) > 1:
                                        os.remove(jpeg)
                                        filenames.remove(jpeg)
                            except Exception as e:
                                self.logger.error("gphoto camera failed to set current image", exc_info=e, stack_info=True)

                            if filename:
                                # return the filenames of the spooled images if files were requestsed.
                                return filenames
                            else:
                                # otherwise remove the temporary files that we created in order to fill self._image
                                for fp in filenames:
                                    os.remove(fp)
                                # and return self._image
                                return self._image

                except subprocess.CalledProcessError as e:
                    self.logger.error("gphoto camera capture subprocess failed number {}".format(tries),
                                      exc_info=e)

                    # self.logger.error(e.output)
                    # for line in e.output.splitlines():
                    #     if not line.strip() == "" and "***" not in line:
                    #         self.logger.error(line.strip())
            else:
                self.logger.critical("gphoto camera capture too many tries")
                if filename:
                    return None
            return None

    @property
    def serial_number(self) -> str:
        """
        returns the current serialnumber for the camera.
        """
        return self._serialnumber

    def focus(self):
        """
        this is meant to trigger the autofocus. currently not in use because it causes some distortion in the images.
        """
        pass
