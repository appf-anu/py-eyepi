# py-eyepi
tool to capture images from dslrs and raspberry pi cameras

### Gphoto2 Serial Numbers
Gphoto2 serial numbers are unique identifiers for DSLR cameras.

they can be acquired by using `gphoto2 --auto-detect` to get the current port of the camera (usually something like "usb:003,002") and then running `gphoto2 --get-config serialnumber --port usb:003,002`
